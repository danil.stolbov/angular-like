const express = require('express');
const app = express();
const path = require('path');

app.use('/js', express.static(path.join(__dirname, '/src')));
app.use('/', express.static(path.join(__dirname)));


app.post('/ping', function(req, res) {
  res.send('pong');
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
